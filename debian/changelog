libmasonx-request-withapachesession-perl (0.31-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 21:57:14 +0100

libmasonx-request-withapachesession-perl (0.31-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 16:26:09 +0100

libmasonx-request-withapachesession-perl (0.31-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 18:30:30 +0100

libmasonx-request-withapachesession-perl (0.31-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    (From 2008, actually.)
  * Update Upstream-Contact in debian/copyright.
  * Drop patches 01-subrequest-return.patch, 02-cgi-request.patch.
    Merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Jul 2015 18:18:41 +0200

libmasonx-request-withapachesession-perl (0.30-4) unstable; urgency=medium

  * Team upload.
  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was:
    Krzysztof Krzyzaniak (eloy) <eloy@debian.org>); Krzysztof Krzyzaniak
    (eloy) <eloy@debian.org> moved to Uploaders.
  * debian/watch: use metacpan-based URL.
  * Switch to "3.0 (quilt)" source format.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Update build and runtime dependencies.
    - drop unneeded versions
    - drop "perl5" and "perl-modules" (closes: #786680)
  * Mark as autopktestable.
  * Bump debhelper compatibility level to 9.
  * Convert dpatch patches to quilt.
    Remove dpatch fragments from d/{rules,control}.
  * Replace debian/rules with dh(1)-3-liner.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 May 2015 16:43:08 +0200

libmasonx-request-withapachesession-perl (0.30-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use Digest::SHA instead of Digest::SHA1
    libdigest-sha1-perl package was removed from Debian. Digest::SHA is part of
    Perl core modules included with the perl interpreter since 5.10.
    Thanks to Ansgar Burchardt <ansgar@debian.org> (Closes: #694474)
  * Don't create .packlist file.
    Add 'create_packlist=0' in config-stamp target to perl Build.PL call.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 01 Dec 2012 09:53:08 +0100

libmasonx-request-withapachesession-perl (0.30-3) unstable; urgency=low

  * debian/control - removed libapache-request-perl from dependencies
    (closes: #444560)
  * patch from Adrian Irving-Beer <wisq-deb@wisq.net> applied
    (closes: #424688)
  * debian/control - updated to be complaint with recent Standards

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sat, 29 Sep 2007 15:52:25 +0200

libmasonx-request-withapachesession-perl (0.30-2) unstable; urgency=low

  * patch from Adrian Irving-Beer <wisq@wisq.net> applied
    (closes: #328399) (closes: #326456)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 18 Nov 2005 16:25:59 +0100

libmasonx-request-withapachesession-perl (0.30-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sat,  4 Sep 2004 22:53:13 +0200

libmasonx-request-withapachesession-perl (0.25-2) unstable; urgency=low

  * debian/control - Changed Section to  perl

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  5 Mar 2004 10:57:53 +0100

libmasonx-request-withapachesession-perl (0.25-1) unstable; urgency=low

  * New upstream release
  * debian/control - libapache-mod-perl2 added to dependency
  * debian/control - Standards-Version updated to 3.6.1.0

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  5 Mar 2004 10:30:18 +0100

libmasonx-request-withapachesession-perl (0.24-4) unstable; urgency=low

  * debian/rules: more polishing, _build directory is removed in section
    cleaning

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sun, 16 Nov 2003 22:54:29 +0100

libmasonx-request-withapachesession-perl (0.24-3) unstable; urgency=low

  * debian/rules: Build file is removed in section clean
    (Closes: #221066)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sun, 16 Nov 2003 22:48:56 +0100

libmasonx-request-withapachesession-perl (0.24-2) unstable; urgency=low

  * debian/control: Section changed from interpreters to perl

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu,  6 Nov 2003 15:38:20 +0100

libmasonx-request-withapachesession-perl (0.24-1) unstable; urgency=low

  * New upstream release
  * debian/rules - rewritten for use Module::Build
  * debian/control - added dependency from Module::Build

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu,  6 Nov 2003 10:40:13 +0100

libmasonx-request-withapachesession-perl (0.21-1) unstable; urgency=low

  * Initial Release.
  * This package is made to replace bad-named-package
    libmasonx-request-withapachesession. I know - it's my fault but this
    name is so long that I didn't notice wrong name. Shame on me!
    Thank Stephen Quinney <sjq@debian.org> for filling bug!

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 23 Jun 2003 14:09:22 +0200
